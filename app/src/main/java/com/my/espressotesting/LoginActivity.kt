package com.my.espressotesting

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val validUsername = "user1234"
    private val validPassword = "1111"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_submit.setOnClickListener {
            tv_login.text =
                if (et_username.text.toString() == validUsername && et_password.text.toString() == validPassword)
                    getString(R.string.label_success)
                else getString(R.string.label_failed)
        }
    }
}